import React from 'react';
import Link from 'next/link';
import { Button } from '@mui/material';

const staticNewsList = [
  { id: 1, title: 'Noticia 1', date: '2024-01-20', location: 'Ciudad A' },
  { id: 2, title: 'Noticia 2', date: '2024-01-21', location: 'Ciudad B' },
  { id: 3, title: 'Noticia 3', date: '2024-01-22', location: 'Ciudad C' },
];

const NewsList: React.FC = () => {
  return (
    <div>
      <h2>Lista de Noticias</h2>
      {staticNewsList.map((item) => (
        <div key={item.id}>
          <h3>{item.title}</h3>
          <p>Fecha: {item.date}</p>
          <p>Lugar: {item.location}</p>
          <Link href={`/news/${item.id}`}>
            <Button variant="contained" color="primary" style={{ marginTop: '16px' }}>
              Ver Detalles
            </Button>
          </Link>
        </div>
      ))}
    </div>
  );
};

export default NewsList;
