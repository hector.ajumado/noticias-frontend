import React from 'react';
import NewsList from './NewsList';

const Page: React.FC = () => {
  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
      {}
      <div className="mb-32 grid text-center lg:max-w-5xl lg:w-full lg:mb-0 lg:grid-cols-4 lg:text-left">
        {}
        <NewsList />
      </div>
      {}
    </main>
  );
};

export default Page;
