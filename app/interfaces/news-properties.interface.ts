interface NewsProperties {
    id: number;
    title: string;
    image?: string;
    date: string;
    location: string;
    author: string;
    content: string;
  }
  
  export default NewsProperties;
  
  