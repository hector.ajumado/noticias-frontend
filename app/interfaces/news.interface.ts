import NewsProperties from './news-properties.interface';

interface News extends NewsProperties {}

export default News;
