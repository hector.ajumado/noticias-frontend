import React from 'react';
import { Typography, Button, Card, CardContent } from '@mui/material';
import Link from 'next/link';
import NewsProperties from '../../interfaces/news-properties.interface';

const NewsList: React.FC<{ news: NewsProperties[] }> = ({ news }) => {
  return (
    <div>
      <Typography variant="h4" gutterBottom>
        Lista de Noticias
      </Typography>

      {news.map((item) => (
        <Card key={item.id} style={{ marginBottom: '16px' }}>
          <CardContent>
            <Typography variant="h5" component="div">
              {item.title}
            </Typography>
            <Typography color="text.secondary">{item.date}</Typography>
            <Typography color="text.secondary">{item.location}</Typography>

            <Link href={`/news/${item.id}`}>
              <Button variant="contained" color="primary" style={{ marginTop: '16px' }}>
                Ver Detalles
              </Button>
            </Link>
          </CardContent>
        </Card>
      ))}
    </div>
  );
};

export default NewsList;
