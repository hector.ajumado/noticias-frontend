import React from 'react';
import { Typography, Button, Card, CardContent } from '@mui/material';
import { useRouter } from 'next/router';
import NewsProperties from '../../interfaces/news-properties.interface';

const NewsDetails: React.FC<{ news: NewsProperties }> = ({ news }) => {
  const router = useRouter();
  const { id } = router.query;

  return (
    <div>
      <Typography variant="h4" gutterBottom>
        Detalles de la Noticia
      </Typography>

      <Card>
        <CardContent>
          <Typography variant="h5" component="div">
            {news.title}
          </Typography>
          <Typography color="text.secondary">{news.date}</Typography>
          <Typography color="text.secondary">{news.location}</Typography>
          <Typography color="text.secondary">{news.author}</Typography>
          <Typography variant="body2" color="text.secondary">
            {news.content}
          </Typography>

          <Button variant="contained" color="primary" onClick={() => router.push('/news')}>
            Volver a la Lista
          </Button>
        </CardContent>
      </Card>
    </div>
  );
};

export default NewsDetails;
