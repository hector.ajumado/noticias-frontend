import React from 'react';
import { Typography, Button, TextField, Container } from '@mui/material';
import { useRouter } from 'next/router';
import NewsProperties from '../../interfaces/news-properties.interface';

const CreateEditNews: React.FC<{ news?: NewsProperties }> = ({ news }) => {
  const router = useRouter();

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    router.push('/news');
  };

  return (
    <Container>
      <Typography variant="h4" gutterBottom>
        {news ? 'Editar Noticia' : 'Crear Noticia'}
      </Typography>

      <form onSubmit={handleSubmit}>
        <TextField label="Título" fullWidth defaultValue={news?.title} required />
        <TextField label="Fecha" fullWidth defaultValue={news?.date} required />
        <TextField label="Ubicación" fullWidth defaultValue={news?.location} required />
        <TextField label="Autor" fullWidth defaultValue={news?.author} required />
        <TextField label="Contenido" fullWidth multiline defaultValue={news?.content} required />

        <Button type="submit" variant="contained" color="primary" style={{ marginTop: '16px' }}>
          {news ? 'Guardar Cambios' : 'Crear Noticia'}
        </Button>
      </form>
    </Container>
  );
};

export default CreateEditNews;
